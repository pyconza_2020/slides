---
title: Laying some of the foundations so your fellow developers can start on the ground floor
author: Kim van Wyk
date: 8 October 2020
institute: PyconZA 2020
colortheme: crane
theme: Pittsburgh
urlcolor: red
linkstyle: bold
fonttheme: "professionalfonts"
---

# What is this about?

* Developers frequently need to write scripts and tools
* Setting up to work with company infrastructure can be:
  * Time consuming
  * Difficult to get right
  * Inconsistent
  * Unaware of available options
* Infrastructure can include:
  * Docker
  * Versioning standards
  * CI/CD Tooling
  * Staging and prod environments

# Some of the many tools available

* There are many tools available to address these needs
  * (I'm not suggesting that these are better than other options)

* Tools used in this talk:
  * [Cookiecutter](https://cookiecutter.readthedocs.io/) to take a bit of user input and generate a set of project files and directories
  * [Poetry](https://python-poetry.org/) for managing isolated virtual environments, package dependencies and package building and publishing
  * [etcd](https://etcd.io/) to track configuration across more than one build environment, such as local development and building through a Continuous Integration tool
  * [Gitlab](http://gitlab.com/) to provide Continuous Integration for automated package and Docker container building in a consistent way across projects
  * [Harbor](https://goharbor.io/) to provide a local Docker registry

Code is all in [this public Gitlab group](https://gitlab.com/pyconza_2020).

Locally running Docker images were used to provide Gitlab, Harbor and etcd for the demos in this talk.

# Laying the foundation

* A cookiecutter template can rapidly set up a directory with expected naming standards, scripts and config
* This talk will use this [simple Python example](https://gitlab.com/pyconza_2020/cookiecutter-python), to:
  * Create a base project directory and subdirectories
  * Create and initialise a new git project
  * Configure poetry to:
    * use a local Python package index and PyPI cache
    * include required 3rd party packages
    * Use a consistent version number scheme
  * Provide a basic Dockerfile ([using this publicly available base image](https://gitlab.com/kimvanwyk/python3-poetry-container))
  * Configure CI/CD tooling
  * Provide a custom Docker building tool

\begin{center}
\textbf{[Demonstration goes here]}
\end{center}
 
# Gitlab group

![](images/01_01_empty_gitlab_group.png)

# Demo directory

![](images/01_02_empty_folder.png)

# Cookiecutter questions

![](images/01_03_cookiecutter_questions.png)

# Empty git project

![](images/01_04_empty_git_project.png)

# Cookiecutter done

![](images/01_05_cookiecutter_done.png)

# Git repo

![](images/01_06_git_repo.png)

# Resulting directory

![](images/01_07_directory_listing.png)

# README and Dockerfile

![](images/01_08_readme_dockerfile.png)

# Python directory

![](images/01_09_python_directory.png)

# Python 3 poetry Dockerfile

![](images/01_10_base_dockerfile.png)

# Docker build support

* For this example, some common information should be in all company Docker images
* Building locally and building via CI/CD should give the same results
* Locally built images should not get to the registry

This [simple example Docker builder](https://gitlab.com/pyconza_2020/docker-build) will:

  * Extract the image version from the pyproject.toml file or the local etcd
  * Apply a consistent set of useful labels
  * Apply a tag to a local build which the registry will reject if pushed
  * Push built images where appropriate
  * Update the version in the local etcd

The example code used is [in this Gitlab repo](https://gitlab.com/pyconza_2020/dtr-image-reporter)

\begin{center}
\textbf{[Demonstration goes here]}
\end{center}
 
# Script source and poetry installs

![](images/02_01_source_and_poetry.png)

# Script executed

![](images/02_02_executed_python.png)

# Local Docker build 1

![](images/02_03_docker_build_1.png)

# Local Docker build 2

![](images/02_04_docker_build_2.png)

# Local Docker execution

![](images/02_050_local_docker_execute.png)

# Local push

![](images/02_05_local_push_failed.png)

# Gitlab CI/CD

![](images/02_06_git_commit_and_push.png)

# CI/CD passed

![](images/02_07_pipeline_passed.png)

# Docker registry updated

![](images/02_10_in_dtr_staging.png)

# Docker promotion support

* It's often desirable to control what can go into prod
* Audit trails of tickets and approvals are also often required
* One fairly simple solution for Docker containers:
  * Only deploy to prod from a specific registry project
  * Only promote images *into* the prod project after an approval process
* This [Docker promotion tool](https://gitlab.com/pyconza_2020/docker-promote) is a simple example

\begin{center}
\textbf{[Demonstration goes here]}
\end{center}
 
# Docker promote help

![](images/03_01_docker_promote_help.png)

# Docker promote executed

![](images/03_02_docker_promoted.png)

# Docker registry updated

![](images/03_03_dtr_prod.png)

# Centralised versioning

* Some projects don't carry version info locally
* entries in etcd can be used instead, keyed by project name
* this [simple bash example](https://gitlab.com/pyconza_2020/cookiecutter-bash) uses this approach

\begin{center}
\textbf{[Demonstration goes here]}
\end{center}
 
# Cookiecutter questions

![](images/04_01_cookiecutter_questions.png)

# Directory and Dockerfile

![](images/04_02_directory_and_dockerfile.png)

# Script execution

![](images/04_03_code_and_execute.png)

# Local Docker build

![](images/04_04_local_docker_build.png)

# Local Docker execution

![](images/04_05_docker_execute_and_commit.png)

# CI/CD passed

![](images/04_06_gitlab_ci_success.png)

# Docker registry updated

![](images/04_07_in_dtr.png)

# Lessons learned during implementation

* For developers who only occasionally write scripts, mechanisms like these need to be regularly advertised
* Can be tempting to get too fancy or complicated
* Helpful if more than one person knows how these tools work under the hood